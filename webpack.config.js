const path = require('path');
const {merge} = require('webpack-merge');

const config = {};

config.base = {
  entry: "./src/ImageToolkit.js",
  output: {
    path: path.join(__dirname, 'dist'),
    library: 'ImageToolkit',
    libraryTarget: 'umd',
    filename: "image-toolkit.min.js",
  },
  externals: {
    quill: 'quill',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.join(__dirname, 'src'),
        exclude: /(node_modules|bower_components)/,
        use: [{
          loader: 'babel-loader',
          options: {
            "presets": [["@babel/preset-env", { "modules": false }]],
            "plugins": ["@babel/plugin-proposal-class-properties"],
          },
        }],
      },
      {
        test: /\.svg$/,
        use: [{
          loader: 'raw-loader',
        }],
      },
    ],
  },
};

config.dev = {
  mode: 'development',
  devtool: 'eval-cheap-module-source-map',
  cache: false,
};

config.prod = {
  mode: 'production',
  optimization: {
    usedExports: true,
  },
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
};

module.exports = env => merge(config.base, env && env.dev ? config.dev : config.prod)
