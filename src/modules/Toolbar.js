import IconFloatLeft from 'quill/assets/icons/float-left.svg';
import IconFloatCenter from 'quill/assets/icons/float-center.svg';
import IconFloatRight from 'quill/assets/icons/float-right.svg';
import IconIndent from 'quill/assets/icons/indent.svg';
import { BaseModule } from './BaseModule';
import debounce from 'debounce';

const Quill = window.Quill || require('quill');
const Parchment = Quill.imports.parchment;
const FloatStyle = new Parchment.Attributor.Style('float', 'float');
const MarginStyle = new Parchment.Attributor.Style('margin', 'margin');
const DisplayStyle = new Parchment.Attributor.Style('display', 'display');
const MaxWidthStyle = new Parchment.Attributor.Style('max-width', 'max-width');
const WidthStyle = new Parchment.Attributor.Style('width', 'width');

const offsetAttributor = new Parchment.Attributor.Attribute('nameClass', 'class', {
  scope: Parchment.Scope.INLINE,
});

Quill.register(offsetAttributor);

export class Toolbar extends BaseModule {
  onCreate = () => {
    // Setup Toolbar
    this.tooltip = document.createElement('div');
    this.tooltip.className = 'ql-tooltip';
    Object.assign(this.tooltip.style, this.options.tooltipStyles);
    this.overlay.appendChild(this.tooltip);

    this.toolbar = document.createElement('div');
    this.toolbar.className = 'ql-toolbar';
    Object.assign(this.toolbar.style, this.options.toolbarStyles);
    this.tooltip.appendChild(this.toolbar);

    this.tooltipArrow = document.createElement('div');
    this.tooltipArrow.className = 'ql-tooltip-arrow';
    Object.assign(this.tooltipArrow.style, this.options.tooltipArrowStyles);
    this.tooltip.appendChild(this.tooltipArrow);

    if(WidthStyle.value(this.img) === ''){
      WidthStyle.add(this.img, this.img.naturalWidth+'px');
      MaxWidthStyle.add(this.img, '100%');
    }

    // Setup Buttons
    this._defineAlignments();
    this._addToolbarButtons();

    this._onUpdateDebounce = debounce(this._onUpdate, 50);
  };

  // The toolbar and its children will be destroyed when the overlay is removed
  onDestroy = () => {};

  // Nothing to update on drag because we are are positioned relative to the overlay
  onUpdate = () => {
    this._onUpdateDebounce();
  };

  _onUpdate = () => {
    const oldMargin = parseInt(this.tooltip.style.marginLeft || 0);
    const tooltipRect = this.tooltip.getBoundingClientRect();
    const imgRect = this.img.getBoundingClientRect();
    const editor = this.quill.root;
    const editorRect = editor.getBoundingClientRect();
    const editorLeftPadding = parseInt(window.getComputedStyle(editor, null).getPropertyValue('padding-left') || 0);

    let newMargin = oldMargin;

    if(editorRect.left + editorRect.width * 0.75 < tooltipRect.left + tooltipRect.width - oldMargin){
      newMargin = Math.min(0, Math.floor((editorRect.left + editorRect.width - editorLeftPadding) - (tooltipRect.left + tooltipRect.width - oldMargin)));
    } else {
      newMargin = Math.max(0, Math.ceil((editorRect.left + editorLeftPadding) - (tooltipRect.left - oldMargin)));
    }

    this.tooltip.style.marginLeft = newMargin+'px';

    const imgCenter = imgRect.left + imgRect.width / 2;
    const tooltipCenter = tooltipRect.left + (newMargin - oldMargin) + tooltipRect.width / 2;
    const limit = tooltipRect.width / 2 - 20;
    this.tooltipArrow.style.marginLeft = Math.max(-limit, Math.min(limit, -(tooltipCenter - imgCenter))) + 'px';

    this.buttons.forEach(btn => {
      this._selectButton(btn, btn.alignment.isApplied());
    });
  };

  _defineAlignments = () => {
    this.alignments = [
      [
        {
          tooltip: 'Inline (like a word in a sentance)',
          icon: IconIndent,
          apply: () => {
            DisplayStyle.remove(this.img);
            FloatStyle.remove(this.img);
            MarginStyle.remove(this.img);
            MaxWidthStyle.add(this.img, '100%');
          },
          reset: [MarginStyle, (img) => MaxWidthStyle.add(img, '100%')],
          isApplied: () => FloatStyle.value(this.img) == '' && DisplayStyle.value(this.img) == '',
        },
        {
          tooltip: 'Align left (text wraps around)',
          icon: IconFloatLeft,
          apply: () => {
            DisplayStyle.add(this.img, 'inline');
            FloatStyle.add(this.img, 'left');
            MarginStyle.add(this.img, '0 1em 1em 0');
            MaxWidthStyle.add(this.img, '50%');
          },
          reset: [DisplayStyle, FloatStyle, MarginStyle, (img) => MaxWidthStyle.add(img, '100%')],
          isApplied: () => FloatStyle.value(this.img) == 'left',
        },
        {
          tooltip: 'Align center (text doesn\'t wrap around)',
          icon: IconFloatCenter,
          apply: () => {
            DisplayStyle.add(this.img, 'block');
            FloatStyle.remove(this.img);
            MarginStyle.add(this.img, '1em auto');
            MaxWidthStyle.add(this.img, '100%');
          },
          reset: [DisplayStyle, FloatStyle, MarginStyle, (img) => MaxWidthStyle.add(img, '100%')],
          isApplied: () => DisplayStyle.value(this.img) == 'block',
        },
        {
          tooltip: 'Align right (text wraps around)',
          icon: IconFloatRight,
          apply: () => {
            DisplayStyle.add(this.img, 'inline');
            FloatStyle.add(this.img, 'right');
            MarginStyle.add(this.img, '0 0 1em 1em');
            MaxWidthStyle.add(this.img, '50%');
          },
          reset: [DisplayStyle, FloatStyle, MarginStyle, (img) => MaxWidthStyle.add(img, '100%')],
          isApplied: () => FloatStyle.value(this.img) == 'right',
        },
      ],
      [
        {
          tooltip: 'Original size',
          icon: '100%',
          style: {width: 'auto', whiteSpace: 'nowrap', lineHeight: 1, fontSize: '14px'},
          apply: () => {
            WidthStyle.add(this.img, this.img.naturalWidth+'px');
            MaxWidthStyle.add(this.img, '100%');
          },
          isApplied: () => WidthStyle.value(this.img) == this.img.naturalWidth+'px',
        },
        {
          tooltip: 'Half size (best for HiDPI screens)',
          icon: '50%',
          style: {width: 'auto', whiteSpace: 'nowrap', lineHeight: 1, fontSize: '14px'},
          apply: () => {
            WidthStyle.add(this.img, Math.floor(this.img.naturalWidth/2)+'px');
            MaxWidthStyle.add(this.img, '100%');
          },
          reset: [WidthStyle, img => MaxWidthStyle.add(img, '100%')],
          isApplied: () => WidthStyle.value(this.img) == Math.floor(this.img.naturalWidth/2)+'px',
        },
      ],
      [
        {
          tooltip: 'Remove image from content',
          icon: 'X',
          style: {width: 'auto', whiteSpace: 'nowrap', lineHeight: 1, fontSize: '14px'},
          apply: () => {
            this.img.remove();
            this.hide();
          },
          reset: [],
          isApplied: () => false,
        },
      ],
    ];
  };

  _addToolbarButtons = () => {
    this.buttons = [];
    this.alignments.forEach((buttonGroup) => {
      const groupWrapper = document.createElement('div');
      groupWrapper.className = 'ql-formats';
      Object.assign(groupWrapper.style, this.options.toolbarGroupStyles);
      this.toolbar.appendChild(groupWrapper);

      buttonGroup.forEach((alignment) => {
        const button = document.createElement('button');
        this.buttons.push(button);
        button.innerHTML = alignment.icon;

        button.alignment = alignment;
        this._selectButton(button, alignment.isApplied());

        if(alignment.tooltip){
          button.title = alignment.tooltip;
        }

        if(alignment.style){
          Object.assign(button.style, alignment.style);
        }

        button.addEventListener('click', () => {
          if (alignment.isApplied()) {
            // If applied, unapply
            alignment.reset.forEach(reset => {
              if(reset instanceof Parchment.Attributor.Style){
                reset.remove(this.img);
              } else if(typeof reset === 'function'){
                reset(this.img);
              }
            });
          } else {
            // otherwise apply
            alignment.apply();
          }

          this.buttons.forEach(btn => {
            this._selectButton(btn, btn.alignment.isApplied());
          });

          // image may change position; redraw drag handles
          this.requestUpdate();
        });
        Object.assign(button.style, this.options.toolbarButtonStyles);
        // if (idx > 0) {
        //   button.style.borderLeftWidth = '0';
        // }
        if(button.children[0] && button.children[0].tagName.toLowerCase() === 'svg'){
          Object.assign(button.children[0].style, this.options.toolbarButtonSvgStyles);
        }
        if (alignment.isApplied()) {
          // select button if previously applied
          this._selectButton(button);
        }
        groupWrapper.appendChild(button);
      });
    });
  };

  _selectButton = (button, active = true) => {
    if(active){
      button.classList.add('ql-active');
      button.style.color = '#fff';
    } else {
      button.classList.remove('ql-active');
      button.style.color = '#ccc';
    }
  };
}
