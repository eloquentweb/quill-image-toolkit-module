# Changelog

## v4.0.0-alpha.2 - January 12, 2020

- Change styles for toolbar buttons

## v4.0.0-alpha.1 - October 20, 2020

- Add Toolbar buttons for resizing and removing image
- Implement Quill's bubble theme

## v3.0.0 - April 9, 2017

- Position drag handles relative to editor, not document
- Simplify toolbar and use floats
- Improved styling

## v2.0.0 - April 3, 2017

- Added toolbar for alignment
- Modularized
- Fix resize bug

## v1.0.0 - March 25, 2017

- Initial version
