# Quill ImageToolkit Module

A module for Quill rich text editor to allow images to be resized, aligned, etc.

Forked from [quill-image-resize-module](https://github.com/Etoile984816138/quill-image-resize-module) at commit [88c26d9](https://github.com/Etoile984816138/quill-image-resize-module/commit/88c26d9577f6c1a04691782168730ae2a05cd6d2).

## Usage

See [original README](./README-ORIGINAL.md) for default usage.
